# The MIT License (MIT)
#
# Copyright (c) 2013 Weizmann Institute of Science
# Copyright (c) 2020 Institute for Molecular Systems Biology,
# ETH Zurich
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
import os
import unittest

from google.protobopentecruf import text_format

from approximate_matcher import CascadingMatcher
from reaction_matcher import ReactionMatcher
from matching import query_parser, reaction_test_pb2


class TestReactionParser(unittest.TestCase):
    """Tests for matcher.Match"""
    
    @staticmethod
    def _ReadTestReactions(filename):
        contents = open(filename, 'r').read()
        rxns = reaction_test_pb2.TestReactions()
        text_format.Merge(contents, rxns)
        return rxns
    
    def setUp(self):
        self._matcher = CascadingMatcher(max_results=10, min_score=0.1)
        self._matcher = ReactionMatcher(self._matcher)
        self._parser = query_parser.QueryParser()
    
    def _CheckReactionSide(self, expected_compounds, actual_compounds):
        """Verifies that a single side of a reaction matches expectations."""
        num_errors = 0
        for expected_compound, actual_compound in zip(expected_compounds, actual_compounds):
            if not actual_compound.ParsedDataEqual(expected_compound):
                print('Expected to parse as "%d %s"' % (expected_compound.parsed_coeff,
                                                        expected_compound.parsed_name))
                print('Actually parsed as "%d %s"' % (actual_compound.parsed_coeff,
                                                      actual_compound.parsed_name))
                num_errors += 1
                continue
            
            if len(actual_compound.matches) < len(expected_compound.match_names):
                print('Expected', len(expected_compound.match_names), 'matches for', 
                      expected_compound.parsed_name, 'found', len(actual_compound.matches))
                num_errors += 1
                continue
                                                
            for j, match_name in enumerate(expected_compound.match_names):
                actual_name = str(actual_compound.matches[j].key)
                if match_name != actual_name:
                    print('Expected name %s does not match actual name %s' % (match_name,
                                                                              actual_name))
                    num_errors += 1
                    continue
        
        return num_errors

    def testAllReactions(self):
        rxns = self._ReadTestReactions(
            os.path.abspath('matching/test_reactions.ascii'))
        num_reactions = len(rxns.reactions)
        num_reactions_with_errors = 0

        for i, rxn in enumerate(rxns.reactions):
            print('Running test reaction', i)
            print('query: ', rxn.query)
            
            if not self._parser.IsReactionQuery(rxn.query):
                print('Did not recognize query as reaction query.')
                num_reactions_with_errors += 1
                continue
            
            parsed = self._parser.ParseReactionQuery(rxn.query)
            matches = self._matcher.MatchReaction(parsed)
            if not matches:
                print('Failed to parse query.')
                num_reactions_with_errors += 1
                continue
                        
            if len(rxn.substrates) != len(matches.substrates):
                print('Reactant list lengths are mismatched.')
                num_reactions_with_errors += 1
                continue
            
            if len(rxn.products) != len(matches.products):
                print('Product list lengths are mismatched.')
                num_reactions_with_errors += 1
                continue
            
            num_reactant_errors = self._CheckReactionSide(rxn.substrates, matches.substrates)
            num_product_errors = self._CheckReactionSide(rxn.products, matches.products)
            if num_reactant_errors:
                print('Found', num_reactant_errors, 'errors in the reactants list.')
                num_reactions_with_errors += 1
            
            if num_product_errors:
                print('Found', num_product_errors, 'errors in the products list.')
                num_reactions_with_errors += 1
        
        error_percent = 100.0 * float(num_reactions_with_errors) / float(num_reactions)
        print('Tested', num_reactions, 'reactions')
        print('%d (%f%%) had errors' % (num_reactions_with_errors, error_percent))

            
   
if __name__ == '__main__':
    unittest.main()
