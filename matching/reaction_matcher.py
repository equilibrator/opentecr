# The MIT License (MIT)
#
# Copyright (c) 2013 Weizmann Institute of Science
# Copyright (c) 2020 Institute for Molecular Systems Biology,
# ETH Zurich
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

from typing import Tuple, List, Union, Optional
import logging
import re

from django.apps import apps

from util import constants
from matching import matcher


class ReactionCompoundMatch(object):
    """A match of a compound in a reaction.
    
    Contains the parsed name (what the user entered), the parsed
    stoichiometric coefficient, and a list of matcher.Match objects
    of the potential matches.
    """
    def __init__(
            self,
            parsed_name: str,
            parsed_coeff: float,
            parsed_phase: str,
            matches: List[matcher.Match],
    ) -> None:
        self.parsed_name = parsed_name
        self.parsed_coeff = parsed_coeff
        self.parsed_phase = parsed_phase
        self.matches = matches
    
    def __str__(self) -> str:
        match_str = ', '.join(map(str, self.matches))
        return (
            f"{self.parsed_coeff} {self.parsed_name}({self.parsed_phase}), "
            f"matches: {match_str}"
        )

    def ParsedDataEqual(self, other: "ReactionCompoundMatch") -> bool:
        """Checks if the parsed data (name, coefficient) are equal
           for two ReactionCompoundMathes.
        """
        return (self.parsed_coeff == other.parsed_coeff and
                self.parsed_name == other.parsed_name and
                self.parsed_phase == other.parsed_phase)

    
class ReactionMatches(object):
    """A reaction parsed from a query with all possible matches."""
    
    def __init__(
            self,
            reactants: Optional[List[ReactionCompoundMatch]] = None
    ) -> None:
        """Initialize the ReactionMatches object.
        
        Args:
            reactants: a list of ReactionCompoundMatches for the reactants.
        """
        self.reactants = reactants or []
    
    @staticmethod
    def _FindFirstCompoundMatch(
            matches: List["ReactionMatches"]
    ) -> Union["ReactionMatches", None]:
        """Finds the first match that has a Compound as a value."""
        for m in matches:
            if isinstance(m.value, apps.get_model('gibbs.Compound')):
                return m
        return None
    
    def GetBestMatch(self) -> Union[List[dict], None]:
        """Returns a 2-tuple of product and reactant lists for the
        best match.
        
        Each list is of 3 tuples (coeff, kegg_id, name).
        """
        reactants = []
        for c in self.reactants:
            compound_match = self._FindFirstCompoundMatch(c.matches)
            if not compound_match:
                return None

            reactants.append({'coeff': c.parsed_coeff,
                              'id': compound_match.value.eqapi_id,
                              'phase': c.parsed_phase,
                              'name': compound_match.key})
        
        return reactants
    

class ReactionMatcher(object):
    """Parses reaction queries from users."""
    
    def __init__(self, compound_matcher: matcher.Matcher):
        """Initialize the ReactionMatcher.
        
        Args:
            compound_matcher: a matcher.Matcher object that matches
                individual compounds.
        """
        self._matcher = compound_matcher
    
    def _MakeReactionCompoundMatch(
            self,
            coeff: float,
            name: int
    ) -> ReactionCompoundMatch:
        compound_name, phase_suffix = ReactionMatcher._StripPhases(name)
        logging.debug("Name = %s, phase = %s" % (compound_name, phase_suffix))
        compound_matches = self._matcher.Match(compound_name)
        return ReactionCompoundMatch(
            compound_name,
            coeff,
            phase_suffix,
            compound_matches
        )

    @staticmethod
    def _StripPhases(name: str) -> Tuple[str, str]:
        compound_name = name
        phase_name = None

        m = re.match('(.*)\((aq|s|l|g)\)$', name)
        if m is not None:
            compound_name, phase_subscript = m.groups()
            phase_name = constants.PHASE_SUBSCRIPT_TO_NAME[phase_subscript]

        return compound_name, phase_name
    
    def MatchReaction(
            self,
            parsed_query
    ) -> Union[ReactionMatches, None]:
        """Parse the query as a reaction.
        
        Args:
            parsed_query: query_parser.ParsedReactionQuery object.
        
        Returns:
            An initialized ReactionMatches object.
        """  
        reactants = []
        for coeff, name in parsed_query.substrates:
            reactants.append(self._MakeReactionCompoundMatch(-coeff, name))
        for coeff, name in parsed_query.products:
            reactants.append(self._MakeReactionCompoundMatch(coeff, name))
        
        if not reactants:
            logging.error('Failed to parse reaction.')
            return None
        
        return ReactionMatches(reactants)
