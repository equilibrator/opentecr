# The MIT License (MIT)
#
# Copyright (c) 2020 Weizmann Institute of Science
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
import logging

from django.apps import apps
from django.db import models

from .. import eqapi_Compound, eqapi_Reaction


class OpenTECREntry(models.Model):
    """A reaction stored in the database."""

    # unique positional identifier to avoid duplications (based on part/page/col/table/entry)
    table_position = models.TextField(null=False, unique=True)

    # ID as it appears in the table
    opentecr_id = models.TextField(null=True)

    # a hash string for fast lookup of enzyme names by reaction
    reaction_hash = models.CharField(max_length=128, null=True)

    # EC number
    ec_number = models.TextField(null=True)

    # reference code (shorthand for citation, in the style of the original TECR-DB)
    reference = models.TextField(null=True)

    # the reaction formula as it appears in openTECR
    description = models.TextField(null=True)

    # K
    equilibrium_constant = models.FloatField(null=True)

    # K'
    equilibrium_constant_prime = models.FloatField(null=True)

    # temperature
    temperature = models.FloatField(null=True)

    # pH
    p_h = models.FloatField(null=True)

    # pMg
    p_mg = models.FloatField(null=True)

    # ionic strength
    ionic_strength = models.FloatField(null=True)

    # part
    part = models.IntegerField(null=True)

    # page
    page = models.IntegerField(null=True)

    # columns (lefr or right)
    col = models.CharField(max_length=1, null=True)

    # table from top
    table = models.IntegerField(null=True)

    # entry nr
    entry = models.IntegerField(null=True)

    # enthalpy
    enthalpy = models.FloatField(null=True)

    def __init__(self, *args, **kwargs):
        super(OpenTECREntry, self).__init__(*args, **kwargs)

    def to_dict(self) -> dict:
        return {
            "EC": self.ec_number,
            "reference": self.reference,
            "part": str(self.part),
            "page": str(self.page),
            "col": str(self.col),
            "table": str(self.table),
            "entry": str(self.entry),
            "formula": self.description,
            "pH": str(self.p_h),
            "pMg": str(self.p_mg),
            "I [M]": str(self.ionic_strength),
            "Temp. [K]": str(self.temperature),
            "K": str(self.equilibrium_constant),
            "K'": str(self.equilibrium_constant_prime),
            "ΔH": str(self.enthalpy),
        }