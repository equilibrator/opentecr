# The MIT License (MIT)
#
# Copyright (c) 2020 Weizmann Institute of Science
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
import json
import re
from typing import Iterable, Union

from django.db import models
from django.utils.text import slugify

from util.thumbnail import InChI2Thumbnail

from .. import formula_parser
from .common_name import CommonName


class Compound(models.Model):
    """
    A single compound.
    """

    # The ID of the compound equilibrator-cache.
    eqapi_id = models.IntegerField(db_index=True, unique=True)

    # InChI representation of the compound (needed for generating a thumbnail).
    inchi = models.TextField(null=True)

    # A list of common names of the compound, used for searching.
    common_names = models.ManyToManyField(CommonName, related_name="compounds")

    # If present, this name should always be used.
    preferred_name = models.CharField(max_length=250, null=True)

    # A remark about this compound.
    note = models.TextField(null=True)

    # A link for detailed remarks about this compound.
    details_link = models.URLField(null=True)

    # The chemical formula.
    formula = models.CharField(max_length=500, null=True)

    # The molecular mass.
    mass = models.FloatField(null=True)  # In Daltons.

    # An explanation for when no DeltaG0 estimate is available.
    no_dg_explanation = models.CharField(max_length=2048, blank=True, null=True)

    # A single static parser
    FORMULA_PARSER = formula_parser.FormulaParser()

    def __init__(self, *args, **kwargs):
        super(Compound, self).__init__(*args, **kwargs)
        self._all_species_groups = None
        self._species_group_to_use = None
        self._priority = None

    def HasData(self) -> bool:
        """
        Has enough data to display.
        """
        return (self.mass is not None) and (self.formula is not None)

    def FirstName(self) -> str:
        """
        Return the 'first' name of this compound.

        If a 'preferred_name' is set, returns that. Otherwise, returns
        the first name in the list of common names. Presumes that the
        list of names is in some order.
        """
        if self.preferred_name:
            return self.preferred_name

        return self.common_names.first().name

    def NameSlug(self) -> str:
        """Return a name with no whitespace or dashes.

        Slug will also never begin with a number to avoid
        confusing the reaction parser.
        """
        slug = "C_%s" % slugify(str(self.FirstName()))
        return slug.replace("-", "_")

    def GetStructureThumbnail(self) -> str:
        if self.inchi is None:
            return "error"
        return InChI2Thumbnail(str(self.inchi), output_format="svg") or "error"

    def GetLink(self) -> Union[str, None]:
        """Returns the link to the stand-alone page for this compound."""
        if not self.eqapi_id:
            return None
        return f"/metabolite?compoundId={self.eqapi_id}&"\
                "reactantsPhase=aqueous&reactantsAbundance=1.000&reactantsAbundanceUnit=millimolar"

    def GetHtmlFormattedFormula(self) -> Union[str, None]:
        """Returns the chemical formula with HTML formatted subscripts."""
        if not self.formula:
            return None

        return re.sub(r"(\d+)", r"<sub>\1</sub>", self.formula)

    @property
    def substrate_of(self):
        return self.substrate_for_enzymes.all()

    @property
    def product_of(self):
        return self.product_of_enzymes.all()

    @property
    def cofactor_of(self):
        return self.cofactor_of_enzymes.all()

    @property
    def all_common_names(self) -> Iterable[str]:
        return map(lambda x: x.name, self.common_names.all())

    first_name = property(FirstName)
    name_slug = property(NameSlug)
    html_formula = property(GetHtmlFormattedFormula)
    link = property(GetLink)
    thumbnail = property(GetStructureThumbnail)

    def __str__(self) -> str:
        """Return a single string identifier of this Compound."""
        if self.preferred_name:
            return str(self.preferred_name)
        else:
            return str(self.formula)
