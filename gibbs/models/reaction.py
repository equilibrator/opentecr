# The MIT License (MIT)
#
# Copyright (c) 2024 Weizmann Institute of Science
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
import logging
from collections import namedtuple
import pandas as pd
from typing import Callable, Dict, Iterable, List, Optional, Tuple, Union

from django.apps import apps
from django.db import models
from equilibrator_api import FARADAY, Q_, ComponentContribution
from numpy import abs

from util import constants

from .. import _ccache, _predictor, eqapi_Compound, eqapi_Reaction
from ..forms import ReactionForm

# proton
PROTON_FORMATION = eqapi_Reaction.parse_formula(
    _ccache.get_compound,
    "= kegg:C00080",
)

# H2O
WATER_FORMATION = eqapi_Reaction.parse_formula(
    _ccache.get_compound,
    "= kegg:C00001",
)

# ATP hydrolysis (to ADP)
ATP_REACTION = eqapi_Reaction.parse_formula(
    _ccache.get_compound,
    "kegg:C00002 + kegg:C00001 = kegg:C00008 + kegg:C00009",
)

# CO2 hydriolyis (to bicarbonate)
CO2_REACTION = eqapi_Reaction.parse_formula(
    _ccache.get_compound,
    "kegg:C00011 + kegg:C00001 = kegg:C01353",
)

_compound_names_and_accessions = [
    ("h2o", "metanetx.chemical:MNXM2"),
    ("nad", "metanetx.chemical:MNXM8"),
    ("nadh", "metanetx.chemical:MNXM10"),
    ("pi", "metanetx.chemical:MNXM9"),
    ("coa", "metanetx.chemical:MNXM12"),
    ("fe_2", "metanetx.chemical:MNXM111"),
    ("fe_3", "metanetx.chemical:MNXM196"),
    ("co2", "metanetx.chemical:MNXM13"),
    ("bicarbonate", "metanetx.chemical:MNXM60"),
]

COMP_DICT = {
    name: _ccache.get_compound(accession)
    for name, accession in _compound_names_and_accessions
}

ReactantCoeffName = namedtuple("ReactantCoeffName", "compound coeff name")


class ReactantFormulaMissingError(Exception):
    def __init__(self, c):
        self.compound = c

    def __str__(self):
        return (
            "Cannot test reaction balancing because the reactant"
            " %d does not have a chemical formula" % self.compound.eqapi_id
        )


class Reaction(models.Model):
    """A reaction."""

    def __init__(
        self,
        eqapi_reaction: eqapi_Reaction,
        compound_names: Dict[int, str] = None,
    ):
        """Construction.

        :param eqapi_reaction: a Reaction object.
        :param compound_names: a dictionary from compounds to the names
        that is to be used to display it.
        """
        super(Reaction, self).__init__()

        self._eqapi_reaction = eqapi_reaction
        # find all the Compound objects corresponding to the eqapi_compounds
        # and keep them in a dictionary
        self.compound_names = compound_names or dict()

        self.comp_contrib = ComponentContribution(ccache=_ccache, predictor=_predictor)

        # the following are only used as cache, no need to copy while cloning
        self._catalyzing_enzymes_cache = None

    @property
    def __len__(self):
        return len(self._eqapi_reaction)

    def Clone(self) -> "Reaction":
        """
        Make a copy of the reaction
        """
        logging.debug("Cloning reaction...")
        return Reaction(
            self._eqapi_reaction.clone(), self.compound_names.copy()
        )

    @staticmethod
    def FromForm(
        form: ReactionForm
    ) -> "Reaction":
        """
        Build a reaction object from a ReactionForm.

        Args:
            form :
                a ReactionForm object.

        Returns:
            A Reaction object or None if there's an error.
        """

        compounds = []
        for cid in form.cleaned_reactantsId:
            compound = _ccache.get_compound_by_internal_id(cid)
            if compound is None:
                raise KeyError(f"Could not find a compound with this ID: {cid}")
            compounds.append(compound)
        coefficients = list(form.cleaned_reactantsCoeff)
        reactant_ids = list(form.cleaned_reactantsId)
        reactant_names = list(form.cleaned_reactantsName)
        phases = list(form.cleaned_reactantsPhase)
        abundances = list(form.cleaned_reactantsAbundance)
        abundance_units = list(form.cleaned_reactantsAbundanceUnit)

        if len(compounds) > 1:
            # drop all instances of H+ from the reaction (we use Alberty's framework
            # where protons are buffered in a solution with constant pH).
            # unless H+ is the only compound in this reaction (so we want
            # to present the "formation" page for H+).
            for i, cpd in enumerate(compounds):
                if _ccache.is_proton(cpd):
                    compounds.pop(i)
                    reactant_ids.pop(i)
                    reactant_names.pop(i)
                    coefficients.pop(i)
                    if phases:
                        phases.pop(i)
                        abundances.pop(i)
                        abundance_units.pop(i)

        eqapi_reaction = eqapi_Reaction(
            dict(zip(compounds, coefficients))
        )

        if not phases:
            for cpd in compounds:
                phased_compound, _ = eqapi_reaction.get_phased_compound(cpd)
                phased_compound.abundance = constants.DEFAULT_ABUNDANCE[
                    phased_compound.phase
                ]
        else:
            for cpd, phase, abundance, unit in zip(
                compounds,
                phases,
                abundances,
                abundance_units,
            ):
                phased_compound, _ = eqapi_reaction.get_phased_compound(cpd)

                if _ccache.is_proton(cpd) or _ccache.is_water(cpd):
                    # avoid changing the phase or abundance of H+ or water
                    continue

                if phase != "None" and phase != phased_compound.phase:
                    # change the phase, in this case we also reset the abundance to its
                    # default value in the new phase
                    phased_compound.phase = phase

                if abundance != "None":
                    # set the new abundance to the value given in the form
                    new_abundance = Q_(float(abundance), unit)
                else:
                    # if a new abundance is not given use the default value in the
                    # current phase
                    new_abundance = constants.DEFAULT_ABUNDANCE[
                        phased_compound.phase
                    ]

                if new_abundance and not new_abundance.check(
                    phased_compound.condition.dimensionality
                ):
                    # if the given value doesn't match in terms of units (e.g. if the
                    # phased has changed but the units are still not updated), we reset
                    # the abundance to the default value in the current phase
                    new_abundance = constants.DEFAULT_ABUNDANCE[
                        phased_compound.phase
                    ]

                phased_compound.abundance = new_abundance

        compound_names = dict(zip(reactant_ids, reactant_names))
        return Reaction(eqapi_reaction, compound_names)

    @staticmethod
    def FromIds(
        compound_list: List[Dict[str, str]]
    ):
        """Build a reaction object from a list of compound dictionaries.

        Args:
            compound_list : list
                an iterable of dictionaries of reactants.

        Returns:
            a properly set-up Reaction object or None if there's an error.
        """
        compound_ids = [int(d["id"]) for d in compound_list]
        coefficients = [float(d["coeff"]) for d in compound_list]
        names = [d["name"] for d in compound_list]
        phases = [d["phase"] for d in compound_list]

        compounds = list(map(_ccache.get_compound_by_internal_id, compound_ids))

        for i, cpd in enumerate(compounds):
            if _ccache.is_proton(cpd):
                compounds.pop(i)
                compound_ids.pop(i)
                coefficients.pop(i)
                names.pop(i)
                phases.pop(i)

        eqapi_reaction = eqapi_Reaction(dict(zip(compounds, coefficients)))
        for cpd, phase in zip(compounds, phases):
            if phase is not None:
                eqapi_reaction.set_phase(cpd, phase)
            phased_compound, _ = eqapi_reaction.get_phased_compound(cpd)
            eqapi_reaction.set_abundance(
                cpd, constants.DEFAULT_ABUNDANCE[phased_compound.phase]
            )

        compound_names = dict(zip(compound_ids, names))
        return Reaction(eqapi_reaction, compound_names)

    def Reverse(self) -> "Reaction":
        """Swap the sides of this reaction."""
        logging.debug("Reversing reaction...")
        return Reaction(
            self._eqapi_reaction.reverse(), self.compound_names.copy()
        )

    def get_compound_name(self, eqapi_cpd: eqapi_Compound) -> str:
        if eqapi_cpd.id not in self.compound_names:
            compounds = apps.get_model("gibbs.Compound").objects.filter(
                eqapi_id=eqapi_cpd.id
            )
            assert len(compounds) == 1, f"Could not find {eqapi_cpd} in the database"
            self.compound_names[eqapi_cpd.id] = compounds[0].FirstName()
        return self.compound_names[eqapi_cpd.id]

    def items(self) -> Iterable[Tuple[eqapi_Compound, float]]:
        """Iterate all equilibrator-api reactants."""
        return self._eqapi_reaction.items()

    def _get_one_side_reactant_dicts(
        self, filter_coeff_by: Callable[[float], bool]
    ) -> List[dict]:
        """Return a list of dictionaries with reactant info.

        Parameters
        ----------
        filter_coeff_by : Callable
            a function that filters the stoichiometric
            coefficients. For example, if can select all negative values to get
            the list of all substrate data.

        Returns
        -------
        reactant_dicts : List[dict]
            a list of dictionaries with reactant info.
        """
        reactant_dicts = []
        for compound, coeff in self.items():
            name = self.get_compound_name(compound)
            if filter_coeff_by(coeff):
                if abs(coeff).round(3) != 1:
                    coeff_str = f"{abs(coeff):.3g}"
                else:
                    coeff_str = ""

                phased_cpd, _ = self._eqapi_reaction.get_phased_compound(compound)
                if phased_cpd is not None:
                    phase_shorthand = phased_cpd.phase_shorthand
                else:
                    phase_shorthand = ""

                reactant_dicts.append(
                    {
                        "name": f"{name}{phase_shorthand}",
                        "href": f"/metabolite?compoundId="
                        f"{phased_cpd.id}&reactantsPhase="
                        f"{phased_cpd.phase}&reactantsAbundance=1.000&reactantsAbundanceUnit"
                        f"=millimolar",
                        "title": compound.formula,
                        "coeff": coeff_str,
                    }
                )

        return reactant_dicts

    @property
    def substrates(self) -> List[dict]:
        return self._get_one_side_reactant_dicts(lambda c: c < 0)

    @property
    def products(self) -> List[dict]:
        return self._get_one_side_reactant_dicts(lambda c: c > 0)

    def __str__(self) -> str:
        """
        Simple text reaction representation.
        """
        return self._eqapi_reaction.__str__()

    def GetCompoundData(self) -> List[dict]:
        return self._eqapi_reaction.serialize()

    @property
    def hash_md5(self) -> str:
        if self._eqapi_reaction:
            return self._eqapi_reaction.hash_md5()
        else:
            return ""

    @property
    def special_reaction_warning(self) -> Union[str, bool]:
        def GetLearnMoreLink(faq_mark) -> str:
            return (
                f'</br><a href="/static/classic_rxns/faq.html#{faq_mark}">'
                "Learn more &raquo;</a>"
            )

        compound_list = list(self._eqapi_reaction.keys())

        if self._eqapi_reaction == PROTON_FORMATION:
            return (
                "The formation energy of a proton is defined as 0."
                + GetLearnMoreLink("why-can-t-i-change-the-concentration-of-h-ions")
            )
        elif self._eqapi_reaction == WATER_FORMATION:
            return (
                    "We assume that the concentration of water is fixed in aqueous "
                    "environments." + GetLearnMoreLink(
                    "why-can-t-i-change-the-concentration-of-water"
                )
            )
        elif self._eqapi_reaction == ATP_REACTION:
            return (
                "The &Delta;G' of ATP hydrolysis is highly affected "
                + "by Mg<sup>2+</sup> ions."
                + GetLearnMoreLink("atp-hydrolysis")
            )
        elif self._eqapi_reaction == CO2_REACTION:
            return (
                "You are looking at the &Delta;G' of CO<sub>2</sub> hydration."
                + GetLearnMoreLink("co2-total")
            )
        elif COMP_DICT["fe_2"] in compound_list and COMP_DICT["fe_3"] in compound_list:
            return (
                "Energetics of iron redox reactions depend heavily on the "
                + "chemical forms of iron involved."
                + GetLearnMoreLink("iron-redox")
            )
        elif (
            COMP_DICT["co2"] in compound_list
            and COMP_DICT["bicarbonate"] in compound_list
        ):
            return (
                "One should not use CO<sub>2</sub>(aq) together with "
                + "CO<sub>2</sub>(total) in the same reaction."
                + GetLearnMoreLink("co2-total")
            )
        elif (
            COMP_DICT["co2"] in compound_list
            and self.get_replace_co2_link() is not None
        ):
            return (
                'Did you mean <a href="%s">CO<sub>2</sub>(total)</a>?'
                % self.get_replace_co2_link()
                + GetLearnMoreLink("co2-total")
            )
        else:
            return False

    def _GetAtomDiff(self) -> Dict[str, int]:
        """Returns the net atom counts from this reaction.

        :return: a dictionary from atoms to counts. None if a formula is
        missing.
        """
        return self._eqapi_reaction._get_reaction_atom_bag(minimal_stoichiometry=1e-2)

    def GetHyperlink(self) -> Optional[str]:
        params = []
        for compound, coeff in self.items():
            name = self.get_compound_name(compound)
            phased_compound, _ = self._eqapi_reaction.get_phased_compound(compound)
            params.append(f"reactantsId={compound.id}")
            params.append(f"reactantsName={name}")
            params.append(f"reactantsCoeff={coeff:.3g}")
        return "/reaction?" + "&".join(params)

    def BalanceWithLink(self, compound) -> Optional[str]:
        """Returns a link to balance this reaction with a given compound."""
        other = self.TryBalancing(compound)
        if other is None:
            return None
        return other.GetHyperlink()

    def GetBalanceElectronsLink(self) -> Optional[str]:
        """
        Returns a link to the same reaction,
        balanced by extra NAD+/NADH pairs.
        """
        other = self.BalanceElectrons()
        if other is None:
            return None
        return other.GetHyperlink()

    def get_replace_co2_link(self) -> Optional[str]:
        """
        Returns a link to the same reaction, but with HCO3-
        instead of CO2.
        """
        rxn = self._replace_compound(COMP_DICT["co2"], COMP_DICT["bicarbonate"])
        rxn = rxn.TryBalancing(COMP_DICT["h2o"])
        if rxn is None:
            return None
        return rxn.GetHyperlink()

    @property
    def is_reactant_formula_missing(self) -> bool:
        return self._GetAtomDiff() is None

    @property
    def reactants_with_missing_formula(self) -> Iterable[str]:
        for compound in self._eqapi_reaction.keys():
            if compound.atom_bag is None:
                yield str(compound)

    @property
    def is_empty(self) -> bool:
        if not self._eqapi_reaction:
            return True
        else:
            return self._eqapi_reaction.is_empty()

    @property
    def is_balanced(self) -> bool:
        """Checks if the collection is atom-wise balanced.

        :return: True if the collection is atom-wise balanced.
        """
        return self._eqapi_reaction.is_balanced(
            ignore_atoms=("H",),
        )

    @property
    def is_half_reaction(self) -> bool:
        """Checks if the collection is electron-wise balanced.

        :return: True if the collection is a half-reaction. Note that the
        function will return False if the reaction is not atom-balanced,
        has no formula, or if it is completely balanced including electrons.
        """
        if self.is_balanced:
            return False
        else:
            return self._eqapi_reaction.is_balanced(
                ignore_atoms=(
                    "H",
                    "e-",
                ),
            )

    def _replace_compound(
        self, old_compound: eqapi_Compound, new_compound: eqapi_Compound
    ) -> "Reaction":
        """Replace one compound with another

        :return: The new reaction.
        """
        if old_compound not in self._eqapi_reaction.sparse:
            return self.Clone()

        new_sparse = dict(self._eqapi_reaction.sparse)
        new_sparse[new_compound] = new_sparse[old_compound]
        del new_sparse[old_compound]

        return Reaction(
            eqapi_reaction=eqapi_Reaction(new_sparse),
            compound_names=self.compound_names.copy(),
        )

    def TryBalancing(self, compound: eqapi_Compound) -> "Reaction":
        """Try to balance the reaction with a given compound.

        :return: If the reaction can be balanced, return the
        balanced reaction. Otherwise return None.
        """
        new_eqapi_reaction = self._eqapi_reaction.balance_with_compound(
            compound=compound
        )
        if new_eqapi_reaction is None:
            return None

        return Reaction(
            new_eqapi_reaction, self.compound_names
        )

    def AddCompound(self, compound: eqapi_Compound, coeff: float) -> None:
        """Adds "how_many" of the compound with the given id.

        Args:
            compound: the Compound object.
            coeff: by how much to change the reactant's coefficient.
        """
        self._eqapi_reaction.add_stoichiometry(compound, coeff)

    def _GetElectronDiff(self) -> int:
        atom_bag = self._GetAtomDiff()
        net_electrons = atom_bag.get("e-", 0)
        return net_electrons

    def BalanceElectrons(
        self, acceptor=COMP_DICT["nad"], reduced_acceptor=COMP_DICT["nadh"], n_e=2
    ) -> "Reaction":
        """Try to balance the reaction electons.

        By default acceptor and reduced accepter differ by 2e-.
        """
        net_electrons = self._GetElectronDiff()
        if net_electrons == 0:
            return self

        rxn = self.Clone()
        rxn.AddCompound(acceptor, net_electrons / float(n_e))
        rxn.AddCompound(reduced_acceptor, -net_electrons / float(n_e))
        return rxn

    @staticmethod
    def unbalanced_to_html(atom_bag: List[Tuple[str, int]], electrons: int) -> str:
        if len(atom_bag) == 0 and electrons > 0:
            return f"{electrons:g}e<sup>-</sup>"
        if (
            len(atom_bag) == 1
            and atom_bag[0][0] == "O"
            and electrons == 10 * atom_bag[0][1]
        ):
            # if oxygen and electrons are missing with a ratio of 1 to 10
            # it is an indication that water molecules have been neglected
            return f"{atom_bag[0][1]:g} H<sub>2</sub>O"
        return "".join(
            [
                atom + ("" if coefficient == 1 else f"<sub>{coefficient:g}</sub>")
                for atom, coefficient in atom_bag
            ]
        )

    @property
    def extra_atoms(self) -> Optional[str]:
        try:
            diff = self._GetAtomDiff()
        except ReactantFormulaMissingError:
            return None
        diff.pop("H", 0)
        extra_electrons = max(0, -diff.pop("e-", 0))
        extra_atoms = sorted(
            [
                (atom, -coefficient)
                for atom, coefficient in diff.items()
                if coefficient < 0
            ]
        )
        return Reaction.unbalanced_to_html(extra_atoms, extra_electrons)

    @property
    def missing_atoms(self) -> Optional[str]:
        try:
            diff = self._GetAtomDiff()
        except ReactantFormulaMissingError:
            return None
        diff.pop("H", 0)
        missing_electrons = max(0, diff.pop("e-", 0))
        missing_atoms = sorted(
            [
                (atom, coefficient)
                for atom, coefficient in diff.items()
                if coefficient > 0
            ]
        )
        return Reaction.unbalanced_to_html(missing_atoms, missing_electrons)

    @property
    def extra_electrons(self) -> int:
        try:
            diff = self._GetAtomDiff()
        except ReactantFormulaMissingError:
            return 0
        return max(0, -diff.get("e-", 0))

    @property
    def missing_electrons(self) -> int:
        try:
            diff = self._GetAtomDiff()
        except ReactantFormulaMissingError:
            return 0
        return max(0, diff.get("e-", 0))

    def _GetAllOpenTECREntries(self) -> list:
        """Find all stored reactions matching this reaction (using the hash)."""
        reaction_hash = self.hash_md5
        if not reaction_hash:
            return []
        return apps.get_model("gibbs.OpenTECREntry").objects.filter(
            reaction_hash=reaction_hash
        )

    @property
    def opentecr_hits(self) -> str:
        entries = self._GetAllOpenTECREntries()
        _df = pd.DataFrame.from_records([e.to_dict() for e in entries])
        if _df.shape[0] == 0:
            return "Couldn't find this reaction in the openTECR database"
        else:
            return _df.to_html(classes="thermoDataTable")

    def get_template_data(self) -> dict:
        # generate a list for the reactant whose concentrations can be
        # set by the user
        reactant_data = []
        for compound, coeff in self.items():
            name = self.get_compound_name(compound)
            phased_compound, _ = self._eqapi_reaction.get_phased_compound(compound)
            rdata = {
                "coeff": coeff,
                "compound_id": compound.id,
                "name": name,
            }
            reactant_data.append(rdata)

        template_data = {
            "reaction": self,
            "reactant_data": reactant_data,
            "balance_with_water_link": self.BalanceWithLink(COMP_DICT["h2o"]),
            "balance_with_coa_link": self.BalanceWithLink(COMP_DICT["coa"]),
            "balance_with_pi_link": self.BalanceWithLink(COMP_DICT["pi"]),
            "balance_electrons_link": self.GetBalanceElectronsLink(),
        }

        return template_data

