# The MIT License (MIT)
#
# Copyright (c) 2020 Weizmann Institute of Science
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
import haystack.forms
from django import forms

from util import constants


class ListFormField(forms.MultipleChoiceField):
    """
    A form field for a list of values that are unchecked.

    The Django MultipleChoiceField does *almost* what we want, except
    it validates that each choice is in a supplied list of choices,
    even when that list is empty. We simply override the validation.
    """

    def valid_value(self, value):
        return True



class BaseSearchForm(haystack.forms.SearchForm):
    def _GetWithDefault(self, key, default):
        if key not in self.cleaned_data or self.cleaned_data[key] is None:
            return default
        return self.cleaned_data[key]


class SuggestForm(BaseSearchForm):
    query = forms.CharField(max_length=2048, required=False)
    cleaned_query = property(lambda self: self._GetWithDefault("query", ""))


class SearchForm(BaseSearchForm):
    query = forms.CharField(max_length=2048, required=False)
    p_h = forms.CharField(max_length=32, required=False)
    p_mg = forms.CharField(max_length=32, required=False)
    ionic_strength = forms.CharField(max_length=32, required=False)
    temperature = forms.CharField(max_length=32, required=False)
    e_potential = forms.CharField(max_length=32, required=False)

    # Convenience accessors for clean data with defaults.
    cleaned_query = property(lambda self: self._GetWithDefault("query", ""))


class BaseReactionForm(SearchForm):
    reactantsPhase = forms.MultipleChoiceField(
        required=False, choices=constants.PHASE_CHOICES
    )
    reactantsAbundance = ListFormField(required=False)
    reactantsAbundanceUnit = forms.MultipleChoiceField(
        required=False, choices=constants.ABUNDANCE_UNITS_CHOICES
    )
    cleaned_reactantsPhase = property(
        lambda self: list(self.cleaned_data["reactantsPhase"])
    )
    cleaned_reactantsAbundance = property(
        lambda self: list(self.cleaned_data["reactantsAbundance"])
    )
    cleaned_reactantsAbundanceUnit = property(
        lambda self: list(self.cleaned_data["reactantsAbundanceUnit"])
    )


class ReactionForm(BaseReactionForm):

    reactionId = forms.CharField(required=False)
    reactantsId = ListFormField(required=False)
    reactantsCoeff = ListFormField(required=False)
    reactantsName = ListFormField(required=False)

    # Convenience accessors for clean data with defaults.
    cleaned_reactionId = property(lambda self: list(self.cleaned_data["reactionId"]))
    cleaned_reactantsId = property(
        lambda self: list(map(int, self.cleaned_data["reactantsId"]))
    )
    cleaned_reactantsCoeff = property(
        lambda self: list(map(float, self.cleaned_data["reactantsCoeff"]))
    )
    cleaned_reactantsName = property(
        lambda self: list(self.cleaned_data["reactantsName"])
    )

