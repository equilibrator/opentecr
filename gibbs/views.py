# The MIT License (MIT)
#
# Copyright (c) 2024 Weizmann Institute of Science
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
import logging
import os

from django.apps import apps
from django.http import (
    Http404,
    HttpRequest,
    HttpResponse,
    HttpResponseBadRequest,
)
from django.shortcuts import render
import json

from opentecr.settings import STATIC_ROOT
from gibbs import service_config
from gibbs.forms import (
    ReactionForm,
    SearchForm,
    SuggestForm,
)
from matching.query_parser import ParseError

NO_STRUCTURE_THUMBNAIL = os.path.join(
    STATIC_ROOT, "images", "structure_not_available.png"
)


def index(request: HttpRequest) -> HttpResponse:
    return HttpResponse("Hello, world.")


def ResultsPage(request: HttpRequest) -> HttpResponse:
    """Renders the search results page for a given query."""
    form = SearchForm(request.GET)
    if not form.is_valid():
        raise Http404
    query_parser = service_config.Get().query_parser
    reaction_matcher = service_config.Get().reaction_matcher
    query = form.cleaned_data.get("query", "")
    logging.debug('Query: "%s"', query)
    if not query.strip():
        return render(request, "main.html", {})

    try:
        parsed_reaction = query_parser.ParseReactionQuery(query)
    except ParseError:
        return render(request, "parse_error_page.html")

    reaction_matches = reaction_matcher.MatchReaction(parsed_reaction)
    best_reaction = reaction_matches.GetBestMatch()
    if not best_reaction:
        return render(request, "search_error_page.html")

    rxn = apps.get_model("gibbs.Reaction").FromIds(best_reaction)
    return render(
        request, "reaction_page.html", rxn.get_template_data()
    )


def ReactionPage(request: HttpRequest) -> HttpResponse:
    """Renders a page for a particular reaction."""
    form = ReactionForm(request.GET)
    if not form.is_valid():
        logging.error(form.errors)
        return HttpResponseBadRequest("Invalid reaction form.")
    rxn = apps.get_model("gibbs.Reaction").FromForm(form)
    response = render(request, "reaction_page.html", rxn.get_template_data())
    return response


def SuggestJson(request: HttpRequest) -> HttpResponse:
    """Renders the suggest JSON."""
    form = SuggestForm(request.GET)
    if not form.is_valid():
        logging.error(form.errors)
        raise Http404
    query = form.cleaned_data.get("query", "")
    suggestions = []
    if query:
        matcher = service_config.Get().search_matcher
        matches = matcher.Match(query)
        suggestions = [m.ToDictForJSON() for m in matches]
    output = {"query": query, "suggestions": suggestions}
    json_data = json.dumps(output)
    return HttpResponse(json_data, content_type="application/json")
