# The MIT License (MIT)
#
# Copyright (c) 2020 Weizmann Institute of Science#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

from django.apps import apps
from haystack import indexes

from gibbs.models import CommonName


class CommonNameIndex(indexes.SearchIndex, indexes.Indexable):
    """
    Index for searching CommonNames.
    """

    text = indexes.CharField(document=True, use_template=True)

    # We add this for autocomplete.
    title_autocomplete = indexes.NgramField(model_attr="name")

    def get_model(self):
        return CommonName

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return apps.get_model("gibbs.CommonName").objects.all()
