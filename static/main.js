$(document).ready(function(){

    // Set up autocomplete
    var options = {
      delimiter: /(^|\+|<=>|=>|=|->|<->)+\s*\d*\s*/,
      serviceUrl: '/suggest',
      groupBy: 'cat',
      preventBadQueries: false,
      triggerSelectOnValidInput: false
    };
    var queryField = $('#queryField');
    if (queryField) {
        queryField.autocomplete(options);
    }
    
    // Advanced settings sliders.
    var phSlider = $("#phSlider");
    var pmgSlider = $("#pmgSlider");
    var ionStrengthSlider = $("#ionStrengthSlider");
    var electronPotentialSlider = $("#electronPotentialSlider");
    
    if (phSlider) {
        phSlider.slider({
            min: 4.0,
            max: 10.0,
            step: 0.1,
            value: $("#phField").val(),
            slide: updatePHField,
            change: updatePHField});
    }
    if (pmgSlider) {
        pmgSlider.slider({
            min: 0.0,
            max: 6.0,
            step: 0.1,
            value: $("#pmgField").val(),
            slide: updatePMgField,
            change: updatePMgField});
    }
    if (ionStrengthSlider) {
        ionStrengthSlider.slider({
            min: 0.0,
            max: 0.5,
            step: 0.01,
            value: $("#ionStrengthField").val(),
            slide: updateISField,
            change: updateISField});    
    }
    if (electronPotentialSlider) {
        electronPotentialSlider.slider({
            min: -2,
            max: 2,
            step: 0.001,
            value: $("#electronPotentialField").val(),
            slide: updateERedField,
            change: updateERedField});    
    }
    
    var customConcRadio = $("#customConcRadio");
    if (customConcRadio) {
        toggleCustomConcentrations();
    }
    
    var rxnForm =  $("#rxnForm");
    if (rxnForm) {
        rxnForm.change(toggleCustomConcentrations);        
    }

    // Enable buttons where desired.
    $('.buttonSet').buttonset();
});
