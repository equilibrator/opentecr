import itertools
import json
import logging
import os
import re
import unittest

import django
from django.test import Client

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "opentecr.settings")

class GibbsTester(unittest.TestCase):

    def setUp(self):
        logging.getLogger().setLevel(logging.WARNING)
        django.setup()
        self.client = Client()

    def test_has_data(self):
        from gibbs.models.compound import Compound
        compound = Compound(kegg_id='fake compound')
        self.assertFalse(compound.HasData())
        
        compound.formula = 'C12H22O11'
        self.assertFalse(compound.HasData())

        compound.mass = 0.0
        self.assertFalse(compound.HasData())
        
        compound.mass = 14.5
        self.assertTrue(compound.HasData())

    def test_simple_search(self):
        response = self.client.post('/search?query=ATP')
        self.assertEqual(response.status_code, 200)
        
        html = str(response.content).replace(r'\n', '')

        # match the compound result
        matches = re.findall('<div class="title">(\w+)</div>', html)
        self.assertIn('ATP', matches)
        self.assertIn('dATP', matches)
        self.assertIn('ATPA', matches)
        
    def test_solr_search(self):
        response = self.client.post('/search?query=ATP')
        self.assertEqual(response.status_code, 200)
        
        html = str(response.content).replace(r'\n', '')

        # match the compound result
        matches = re.findall('<div class="title">(\w+)</div>', html)
        
        # the search doesn't work very well with "simple" and these
        # results are not in the matched list.
        self.assertIn('ATP', matches)
        self.assertIn('dATP', matches)
        self.assertIn('ATPA', matches)
        self.assertIn('BzATP', matches)

        # match the enzyme result
        matches = re.findall('<div class="title"><a href="[^"]+">([\w\-]+)</a> \(EC [\d\.]+\)</div>',
                             html)
        self.assertIn('AtPAO3', matches)
        self.assertIn('AtPAO3', matches)
        self.assertIn('ATPase', matches)


if __name__ == "__main__":
    unittest.main()
