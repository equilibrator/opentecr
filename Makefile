# migrate the Compound Cache from Zenodo -> .sqlite file -> PostgreSQL
migrate_compound_cache:
	docker compose -f docker-compose.develop.yml run --rm djangoapp bash scripts/wait-for-postgres.sh "python -m scripts.migrate_compound_cache"

# populate the database tables corresponding to the Django modules, using the raw data files (takes ~2 hours)
build_db:
	docker compose -f docker-compose.develop.yml run --rm djangoapp bash scripts/wait-for-postgres.sh "python -m scripts.init_db_from_rawfiles --noinput"

# generate the sql dump file (which can be used later by 'init_db' for quick initialization)
sqldump:
	docker compose -f docker-compose.develop.yml run --name db_dumper djangoapp bash scripts/wait-for-postgres.sh "pg_dump -Fc -f data/psql.dump"
	docker cp db_dumper:app/src/data/psql.dump ./data/psql.dump
	docker stop db_dumper
	docker rm db_dumper

# load the Django data from the PostgreSQL dumpfile
init_db:
	docker compose -f docker-compose.develop.yml run --rm djangoapp bash scripts/wait-for-postgres.sh "python -m scripts.init_db_from_dumpfile data/psql.dump"

# copy the Django static files so they can be served directly with gunicorn
collectstatic:
	docker compose -f docker-compose.develop.yml run --rm djangoapp bash scripts/wait-for-postgres.sh "python manage.py collectstatic --noinput"

init: migrate_compound_cache init_db collectstatic

runserver:
	docker compose -f docker-compose.develop.yml up

down:
	docker compose -f docker-compose.develop.yml down
	
secretkey:
	docker compose -f docker-compose.develop.yml run --rm djangoapp bash scripts/wait-for-postgres.sh "python manage.py generate_secret_key"
	
test:
	docker compose -f docker-compose.develop.yml run --rm djangoapp bash -c "sleep 20 && python -m unittest discover tests"
