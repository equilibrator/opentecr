# The MIT License (MIT)
#
# Copyright (c) 2024 Weizmann Institute of Science
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import argparse
import datetime
import logging
import os
import sys
import time
import django
from django.core.management import execute_from_command_line
from django.db import transaction
from numpy import floor
from sqlalchemy import create_engine
from equilibrator_cache.compound_cache import CompoundCache

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "opentecr.settings")
django.setup()

logging.getLogger().setLevel(logging.INFO)

from util import database_io
from opentecr.settings import DATABASES

db_user = DATABASES["default"]["USER"]
db_host = DATABASES["default"]["HOST"]

############################ MAIN #############################
parser = argparse.ArgumentParser(
    description=('Rebuild openTECR MySQL database')
)
parser.add_argument('--noinput', action='store_true',
                    help='Disable user prompt messages')
args = parser.parse_args()

logging.info(
    f"Welcome to the {__file__} script. "
    "Running it will erase all data in the PostgreSQL database, "
    "and rebuild it from the raw data files."
)
logging.info(" **** NOTE: the whole process can take around 10 minutes ****")
if not args.noinput:
    s = input("Are you sure you want to proceed [y/N]? ")
    if s.lower() not in ["y", "yes"]:
        sys.exit(0)

start = time.time()

logging.info("Connection to compound cache stored on PostgreSQL.")
ccache = CompoundCache(
    create_engine(f"postgresql://{db_user}@{db_host}/compounds")
)

logging.info('> Dropping the existing database')
cmd = "psql --quiet --command=\"DROP SCHEMA public CASCADE;\""
os.system(cmd)

cmd = "psql --quiet --command=\"CREATE SCHEMA public;\""
os.system(cmd)

logging.info('> Migrating DB')
execute_from_command_line(['', 'migrate', '--noinput', '--run-syncdb', '-v', 0])

logging.info('> Flushing DB')
execute_from_command_line(['', 'flush', '--noinput', '-v', 0])

transaction.set_autocommit(False)

logging.info('> Loading compound names')
database_io.load_kegg_compound_names(ccache)
transaction.commit()

logging.info('> Loading openTECR data')
database_io.load_opentecr_data(ccache)
transaction.commit()

end = time.time()
elapsed = datetime.timedelta(seconds=floor(end - start))
logging.info('Elapsed loading time = %s' % str(elapsed))

