# The MIT License (MIT)
#
# Copyright (c) 2013 Weizmann Institute of Science
# Copyright (c) 2020 Institute for Molecular Systems Biology,
# ETH Zurich
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
from typing import Tuple
import gzip
import logging

from pyparsing.exceptions import ParseException
from tqdm import tqdm
from pkg_resources import resource_filename

import pandas as pd
from django.apps import apps
from django.db.utils import DataError, IntegrityError
from equilibrator_api import ComponentContribution
from equilibrator_cache import CompoundCache


COMPOUND_NAMES_DF = pd.read_csv(
    gzip.open(resource_filename(__name__, '../data/compound_names.csv.gz'), 'r'), index_col=None
)
COMPOUND_RENAMING_DF = pd.read_csv(
    gzip.open(resource_filename(__name__, '../data/compound_renaming.csv.gz'), 'r'), index_col=None
)
OPENTECR_DATA_DF = pd.read_csv(
    gzip.open(resource_filename(__name__, '../data/opentecr_data.csv.gz'), 'r'), index_col=None
)

# Cache compounds so we can look them up faster.
CITATIONS_CACHE = {}



def get_compound_names() -> pd.DataFrame:
    names_df = COMPOUND_NAMES_DF.copy()
    names_df["synonyms"] = names_df.synonyms.str.split('|')
    names_df.set_index("compound_id", inplace=True)

    to_delete = []
    for row in COMPOUND_RENAMING_DF.itertuples(index=False):
        if row.command == 'remove':
            # remove 'name' from the list of names
            try:
                names_df.at[row.compound_id, "synonyms"].remove(row.name)
            except ValueError:
                logging.warning(
                    f"The name {row.name} is not one of the options for "
                    f"{row.compound_id}, so it cannot be removed"
                )
        elif row.command == 'add':
            # put 'name' at the end of the list if it doesn't exist already
            if row.name not in names_df.at[row.compound_id, "synonyms"]:
                names_df.at[row.compound_id, "synonyms"].append(row.name)
        elif row.command == 'delete':
            names_df.drop(row.compound_id, axis=0)
        elif row.command == 'replace':
            # move the synonyms from the 'old' compound to the 'new' one
            old_synonyms = names_df.at[row.compound_id, "synonyms"]
            new_synonyms = names_df.at[row.name, "synonyms"]
            new_synonyms += list(set(old_synonyms).difference(new_synonyms))
            names_df.drop(row.compound_id, axis=0)
        else:
            raise ValueError(f"Unknown command: {row.command}")

    names_df.drop(to_delete, axis=0)

    # add the KEGG ID itself as one of the names, this enables us to
    # write down reactions using KEGG IDs in the search bar
    for row in names_df.itertuples(index=True):
        row.synonyms.append(row.Index.replace("kegg:", ""))

    return names_df.reset_index().sort_values("compound_id", axis=0)


def get_or_create_compound(
        ccache: CompoundCache,
        compound_id: str
) -> Tuple["Compound", bool]:
    compound_model = apps.get_model('gibbs.Compound')
    eqapi_compound = ccache.get_compound(compound_id)
    if eqapi_compound is None:
        logging.debug(
            f"Cannot find {compound_id} in equilibrator-cache"
        )
        return None, False

    compound, created = compound_model.objects.get_or_create(
        eqapi_id=eqapi_compound.id
    )
    if created:
        compound.formula = eqapi_compound.formula
        compound.inchi = eqapi_compound.inchi
        compound.mass = eqapi_compound.mass

    return compound, created


def load_kegg_compound_names(ccache: CompoundCache) -> None:
    # TODO: instead of relying on raw TSV files, use the cache itself to
    #  find the relevant names and IDs

    names_df = get_compound_names()
    cname_model = apps.get_model('gibbs.CommonName')
    for row in tqdm(
        names_df.itertuples(index=False),
        total=names_df.shape[0],
        desc="compound names",
    ):
        compound, created = get_or_create_compound(ccache, row.compound_id)
        if compound is None:
            continue

        # If this is a duplicate, does not override the preferred name,
        # but appends all the common names to the list nevertheless
        if not created:
            logging.debug(f"The compound {row.compound_id} is a duplicate")
        else:
            compound.preferred_name = row.preferred_name

        for name in row.synonyms:
            cname, _ = cname_model.objects.get_or_create(name=name,
                                                         enabled=True)
            compound.common_names.add(cname)
        compound.save()


def load_opentecr_data(ccache: CompoundCache) -> None:
    eq_api = ComponentContribution(ccache=ccache)

    tecr_model = apps.get_model('gibbs.OpenTECREntry')

    hash_cache = {}

    for idx, rd in tqdm(OPENTECR_DATA_DF.iterrows(), desc="stored reactions"):
        try:
            part = int(rd['part'])
            page = int(rd['page'])
            col = str(rd['col l/r'])
            table = int(rd['table from top'])
            entry = int(rd['entry nr'])
            table_position = f"part{part}_page{page}_col{col}_table{table}_entry{entry}"
            logging.info("\nPosition: " + table_position)
        except ValueError as e:
            logging.warning(str(e))
            continue

        if tecr_model.objects.filter(table_position=table_position).exists():
            logging.info("This entry already exists in the DB, skipping...")
            continue

        reaction_hash = None
        formula = str(rd["description"])
        if formula not in ["", "nan"]:
            formula = formula.replace("(aq)", "")
            if formula in hash_cache:
                reaction_hash = hash_cache[formula]
                logging.info("found hit for: " + formula)
                logging.info("MD5 hash: " + reaction_hash)
            else:
                logging.info("calculating hash for: " + formula)
                eqapi_reaction = None
                try:
                    eqapi_reaction = eq_api.search_reaction(formula)
                except ParseException as e:
                    logging.warning(str(e))
                except ValueError as e:
                    logging.warning(str(e))

                if eqapi_reaction is not None:
                    reaction_hash = eqapi_reaction.hash_md5()
                    hash_cache[formula] = reaction_hash
                    logging.info("MD5 hash: " + reaction_hash)
                else:
                    logging.info("failed to generate hash")
        try:
            _, _ = tecr_model.objects.get_or_create(
                table_position=table_position,
                reaction_hash=reaction_hash,
                opentecr_id=rd["id"],
                ec_number=rd["EC"],
                reference=rd["reference"],
                description=formula,
                equilibrium_constant=rd["K"],
                equilibrium_constant_prime=rd["K_prime"],
                temperature=rd["temperature"],
                p_h=rd["p_h"],
                p_mg=rd["p_mg"],
                ionic_strength=rd["ionic_strength"],
                part=rd["part"],
                page=rd["page"],
                col=rd["col l/r"],
                table=rd["table from top"],
                entry=rd["entry nr"],
                enthalpy=rd["enthalpy"],
            )
        except IntegrityError as e:
            logging.warning(str(e))
            continue

