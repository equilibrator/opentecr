from equilibrator_api import Q_

POSSIBLE_REACTION_ARROWS = ("=", "=>", "<=>", "->", "<->", u"\u2192", u"\u21CC")

DEFAULT_PHASE = "aqueous"

PH_RANGE_VALUES = [5.0, 5.5, 6.0, 6.5, 7.0, 7.5, 8.0, 8.5, 9.0]

AQUEOUS_PHASE_NAME = "aqueous"
GAS_PHASE_NAME = "gas"
LIQUID_PHASE_NAME = "liquid"
SOLID_PHASE_NAME = "solid"

PHASE_CHOICES = [
    (AQUEOUS_PHASE_NAME, AQUEOUS_PHASE_NAME),
    (SOLID_PHASE_NAME, SOLID_PHASE_NAME),
    (LIQUID_PHASE_NAME, LIQUID_PHASE_NAME),
    (GAS_PHASE_NAME, GAS_PHASE_NAME),
]

PHASE_SUBSCRIPT_TO_NAME = {
    "aq": AQUEOUS_PHASE_NAME,
    "s": SOLID_PHASE_NAME,
    "l": LIQUID_PHASE_NAME,
    "g": GAS_PHASE_NAME,
}

DEFAULT_PHASE = AQUEOUS_PHASE_NAME

ABUNDANCE_UNITS_CHOICES = [
    ("None", "None"),
    ("molar", "molar"),
    ("millimolar", "millimolar"),
    ("micromolar", "micromolar"),
    ("nanomolar", "nanomolar"),
    ("bar", "bar"),
    ("millibar", "millibar"),
    ("microbar", "microbar"),
    ("nanobar", "nanobar"),
]

DEFAULT_ABUNDANCE = {
    AQUEOUS_PHASE_NAME: Q_("1 millimolar"),
    GAS_PHASE_NAME: Q_("1 millibar"),
    SOLID_PHASE_NAME: None,
    LIQUID_PHASE_NAME: None,
}
