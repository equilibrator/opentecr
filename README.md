openTECR
============
A simple search-based website for looking up data from [openTECR](https://github.com/opentecr/opentecr).

openTECR is written in Python using the Django framework.
It was developed primarily on Ubuntu 22.04.4 LTS and is easiest
to develop and set up on that operating system.

The best way to run your own local version of the web server is
using [Docker](https://docs.docker.com/v17.12/install/).

## Running a local web server:
- Make sure that you have `make`, `git` installed (e.g. on Debian/Ubuntu you can use `apt install build-essential git`)
- Make sure you have a recent version of Docker that offers the `compose` command (e.g. follow these [instructions](https://docs.docker.com/compose/install/))
- Clone the repository: `git clone https://gitlab.com/equilibrator/opentecr.git`
- Change to the new directory: `cd opentecr`
- Create the Docker container for the Djangoapp (opentecr):
  - `docker build . --tag <USERNAME>/opentecr:<VERSION>`, where
  `<USERNAME>` is your docker username and `<VERSION>` is up to you to choose.
  - (Optional) if you want to publish the container on dockerhub, run this: `docker push <USERNAME>/opentecr:<VERSION>`
  - change the `docker-compose.develop.yml` file to the version you just created
- Populate the database
  - (Fast option): load the data from the SQL dump file: `make init`
  - (Slow option): load the data from the raw data files: `make db-init`
- Now, the docker containers should be ready, and the data is loaded in the DB.

### To start the server:
- Run the command: `make runserver`
- Browse to this [page](http://localhost:8080)

## Installing on a fresh Amazon AWS Linux instance:
- First, install all the dependencies (git, docker, make):
```bash
sudo yum update -y
sudo yum install docker git make -y
sudo usermod -a -G docker ec2-user
id ec2-user
newgrp docker
sudo curl -L https://github.com/docker/compose/releases/latest/download/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
sudo systemctl enable docker.service
sudo systemctl start docker.service
```
- Then, log in to the docker account:
  * `docker login -e eladnoor -p <PASSWORD>`
- Clone this repository:
  * `git clone https://gitlab.com/equilibrator/opentecr.git`
- Then run the following docker commands from `~/opentecr`:
```bash
docker-compose -f docker-compose.develop.yml run --rm djangoapp bash scripts/wait-for-postgres.sh "python -m scripts.migrate_compound_cache"
docker-compose -f docker-compose.develop.yml run --rm djangoapp bash scripts/wait-for-postgres.sh "python -m scripts.init_db_from_dumpfile data/psql.dump"
docker-compose -f docker-compose.develop.yml run --rm djangoapp bash scripts/wait-for-postgres.sh "python manage.py collectstatic --noinput"
export DOMAIN=<INSTANCE IP>
```

### To start the server:
- Run the command: `docker-compose -f docker-compose.yml up`
- Browse to this [page](http://localhost:8080)

